#!/bin/sh

ssh -o StrictHostKeyChecking=no $SSH_USER@$PRODUCT_HOST_1 << 'ENDSSH'
  set -e
  sudo -i
  chmod 600 /root/.ssh/id_rsa
  cd /home/admin/domains/buonbanonline.vn/public_html/
  rm -Rf data/logs/ref_logs/tmp.log
  git pull origin master
  chown admin:admin /home/admin/domains/buonbanonline.vn/public_html/*
  chown -R admin:admin /home/admin/domains/buonbanonline.vn/public_html/*
  #chmod -R 777 /home/admin/domains/buonbanonline.vn/public_html
  chmod -Rf 755 ./*
  find ./ -type f -exec chmod 644 {} +;
  exec "$@"
ENDSSH
